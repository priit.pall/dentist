package com.cgi.dentistapp.service;

import com.cgi.dentistapp.dto.DentistVisitDTO;
import com.cgi.dentistapp.dto.DentistVisitDetailedDTO;
import com.cgi.dentistapp.dto.DentistVisitListDTO;
import com.cgi.dentistapp.entity.Dentist;
import com.cgi.dentistapp.entity.DentistVisit;
import com.cgi.dentistapp.repostiory.DentistVisitRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class DentistVisitService {

    private final ModelMapper mapper;
    private final DentistVisitRepository repository;
    private final DentistService dentistService;

    public DentistVisitService(ModelMapper mapper, DentistVisitRepository repository,
      DentistService dentistService) {
        this.mapper = mapper;
        this.repository = repository;
        this.dentistService = dentistService;
    }

    public void addVisit(DentistVisitDTO dentistVisitDTO) {
        DentistVisit dentistVisit = mapper.map(dentistVisitDTO, DentistVisit.class);
        Dentist dentist = dentistService.getDentist(dentistVisitDTO.getDentistId());
        dentistVisit.setDentist(dentist);
        repository.save(dentistVisit);
    }

    public List<DentistVisitListDTO> getDentistVisits() {
        List<DentistVisit> dentistVisits = repository.findAll();
        List<DentistVisitListDTO> dtos = new ArrayList<>();
        for (DentistVisit visit : dentistVisits) {
            DentistVisitListDTO dto = mapper.map(visit, DentistVisitListDTO.class);
            dto.setDentistName(visit.getDentist().getName());
            dtos.add(dto);
        }

        return dtos;
    }

    public DentistVisitDetailedDTO getDentistVisit(Long id) {
        DentistVisit dentistVisit = repository.findOne(id);
        DentistVisitDetailedDTO dto = mapper.map(dentistVisit, DentistVisitDetailedDTO.class);
        dto.setDentistId(dentistVisit.getDentist().getId());
        dto.setDentistName(dentistVisit.getDentist().getName());
        return dto;
    }

    public void updateDentistVisit(DentistVisitDetailedDTO dentistVisitDTO, Long id) {
        DentistVisit dentistVisit = mapper.map(dentistVisitDTO, DentistVisit.class);
        Dentist dentist = dentistService.getDentist(dentistVisitDTO.getDentistId());
        dentistVisit.setDentist(dentist);
        dentistVisit.setId(id);

        repository.save(dentistVisit);
    }

    public void deleteDentistVisit(Long id) {
        repository.delete(id);
    }

    public boolean isAppointmentAvailable(Long dentistId, Date visitTime) {
        Optional<DentistVisit> dentistVisit = repository.findByDentist_IdAndVisitTime(dentistId,
          visitTime);
        return !dentistVisit.isPresent();
    }

    public List<DentistVisitListDTO> getAppointments(Pageable pageable) {
        Page<DentistVisit> dentistVisitPage = repository.findAll(pageable);
        List<DentistVisitListDTO> dtos = new ArrayList<>();
        for (DentistVisit visit : dentistVisitPage.getContent()) {
            DentistVisitListDTO dto = mapper.map(visit, DentistVisitListDTO.class);
            dto.setDentistName(visit.getDentist().getName());
            dtos.add(dto);
        }

        return dtos;
    }
}
