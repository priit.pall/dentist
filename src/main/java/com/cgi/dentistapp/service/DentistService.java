package com.cgi.dentistapp.service;

import com.cgi.dentistapp.dto.DentistDTO;
import com.cgi.dentistapp.entity.Dentist;
import com.cgi.dentistapp.repostiory.DentistRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class DentistService {

    private final ModelMapper mapper;
    private final DentistRepository repository;

    public DentistService(DentistRepository repository, ModelMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public List<DentistDTO> getDentists() {
        List<Dentist> dentists = repository.findAll();
        return dentists
          .stream()
          .map(dentist -> mapper.map(dentist, DentistDTO.class))
          .collect(Collectors.toList());
    }

    public Dentist getDentist(Long id) {
        return repository.getOne(id);
    }
}
