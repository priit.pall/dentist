package com.cgi.dentistapp.controller.mvc;

import com.cgi.dentistapp.dto.DentistVisitDTO;
import com.cgi.dentistapp.dto.DentistVisitDetailedDTO;
import com.cgi.dentistapp.service.DentistService;
import com.cgi.dentistapp.service.DentistVisitService;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.validation.Valid;

@Controller
@EnableAutoConfiguration
public class DentistAppController extends WebMvcConfigurerAdapter {

    private final DentistService dentistService;
    private final DentistVisitService dentistVisitService;

    public DentistAppController(DentistService dentistService,
      DentistVisitService dentistVisitService) {
        this.dentistService = dentistService;
        this.dentistVisitService = dentistVisitService;
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/results").setViewName("results");
        registry.addViewController("/appointments").setViewName("appointments");
        registry.addViewController("/show").setViewName("show");
        registry.addViewController("/create").setViewName("create");
    }

    @GetMapping("/")
    public String showCreateView(DentistVisitDTO dentistVisitDTO, Model model) {
        model.addAttribute("dentists", dentistService.getDentists());
        return "create";
    }

    @GetMapping("/appointments")
    public String showAppointmentsView(Model model) {
        model.addAttribute("appointments", dentistVisitService.getDentistVisits());
        return "appointments";
    }

    @GetMapping("/appointments/{id}")
    public String showDetailedView(DentistVisitDetailedDTO dentistVisitDTO, Model model, @PathVariable Long id) {
        model.addAttribute("dentists", dentistService.getDentists());
        model.addAttribute("dentistVisitDetailedDTO", dentistVisitService.getDentistVisit(id));
        return "show";
    }

    @PostMapping("/")
    public String postRegisterForm(@Valid DentistVisitDTO dentistVisitDTO, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("dentists", dentistService.getDentists());
            return "create";
        } else {
            boolean isAppointmentAvailable = dentistVisitService.isAppointmentAvailable(dentistVisitDTO.getDentistId(), dentistVisitDTO.getVisitTime());
            if (isAppointmentAvailable) {
                dentistVisitService.addVisit(dentistVisitDTO);
                return "redirect:/results";
            } else {
                FieldError fieldError = new FieldError("DentistVisitDTO", "visitTime", "Time unavailable");
                bindingResult.addError(fieldError);
                model.addAttribute("dentists", dentistService.getDentists());
                return "create";
            }
        }
    }

    @PutMapping("/appointments/{id}")
    public String putRegisterForm(@Valid DentistVisitDetailedDTO dentistVisitDetailedDTO, BindingResult bindingResult, Model model, @PathVariable Long id) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("dentists", dentistService.getDentists());
            model.addAttribute("dentistVisitDTO", dentistVisitDetailedDTO);
            return "show";
        } else {
            boolean isAppointmentAvailable = dentistVisitService.isAppointmentAvailable(dentistVisitDetailedDTO.getDentistId(), dentistVisitDetailedDTO.getVisitTime());
            if (!isAppointmentAvailable) {
                FieldError fieldError = new FieldError("DentistVisitDTO", "visitTime", "Time unavailable");
                bindingResult.addError(fieldError);
                model.addAttribute("dentists", dentistService.getDentists());
                model.addAttribute("dentistVisitDTO", dentistVisitService.getDentistVisit(id));
                return "show";
            } else {
                dentistVisitService.updateDentistVisit(dentistVisitDetailedDTO, id);
                return "redirect:/appointments";
            }
        }
    }

    @DeleteMapping("/appointments/{id}")
    public String deleteAppointment(@PathVariable Long id) {
        dentistVisitService.deleteDentistVisit(id);
        return "redirect:/appointments";
    }
}
