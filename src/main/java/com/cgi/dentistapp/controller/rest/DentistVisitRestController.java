package com.cgi.dentistapp.controller.rest;

import com.cgi.dentistapp.dto.DentistVisitListDTO;
import com.cgi.dentistapp.service.DentistVisitService;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest")
public class DentistVisitRestController {

    private final DentistVisitService dentistVisitService;

    public DentistVisitRestController(DentistVisitService dentistVisitService) {
        this.dentistVisitService = dentistVisitService;
    }

    @GetMapping("/appointments")
    public ResponseEntity<List<DentistVisitListDTO>> getAppointments(Pageable pageable) {
        return ResponseEntity.ok(dentistVisitService.getAppointments(pageable));
    }
}
