package com.cgi.dentistapp.entity;

import java.util.Date;
import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "dentist_visit")
@Getter
@Setter
public class DentistVisit {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "dentist_id")
    private Dentist dentist;

    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private Date visitTime;

    @Override
    public String toString() {
        return "DentistVisit{" +
          "id=" + id +
          ", dentist=" + dentist +
          ", visitTime=" + visitTime +
          '}';
    }
}
