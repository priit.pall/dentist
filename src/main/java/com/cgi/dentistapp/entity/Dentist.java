package com.cgi.dentistapp.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "dentist")
@Getter
@Setter
public class Dentist {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
}
