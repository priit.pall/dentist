package com.cgi.dentistapp.dto;

import javax.validation.constraints.Future;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Getter
@Setter
public class DentistVisitDTO {
    @NotNull
    private Long dentistId;

    @NotNull
    @Future
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private Date visitTime;
}
