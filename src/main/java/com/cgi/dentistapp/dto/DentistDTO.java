package com.cgi.dentistapp.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DentistDTO {
    private Long id;
    private String name;
}
