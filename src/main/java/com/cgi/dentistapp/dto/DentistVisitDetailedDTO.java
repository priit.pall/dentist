package com.cgi.dentistapp.dto;

import java.util.Date;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

@Getter
@Setter
public class DentistVisitDetailedDTO {
    @NotNull
    private Long id;
    @NotNull
    private Long dentistId;
    private String dentistName;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    @Future
    private Date visitTime;
}
