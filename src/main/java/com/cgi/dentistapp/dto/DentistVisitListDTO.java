package com.cgi.dentistapp.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DentistVisitListDTO {
    private Long id;
    private Long dentistId;
    private String dentistName;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm")
    private Date visitTime;
}
