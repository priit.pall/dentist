package com.cgi.dentistapp.repostiory;

import com.cgi.dentistapp.entity.DentistVisit;
import java.util.Date;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DentistVisitRepository extends JpaRepository<DentistVisit, Long> {
    Optional<DentistVisit> findByDentist_IdAndVisitTime(Long dentist, Date visitTime);
}
